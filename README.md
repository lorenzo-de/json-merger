
## Description

Updates JSON array in one file with JSON object from another file.

### Example

Say you have the following JSON array in file ```data.json```:

```[{title: "first", uuid: 24e22aa8-a069-11e8-98d0-529269fb1459},{title: "second", uuid: 24e22ec2-a069-11e8-98d0-529269fb1459},{title: "third", uuid: 24e23160-a069-11e8-98d0-529269fb1459}]```

If you modified one of the objects, f.e. ```{title: "Formerly known as first", uuid: 24e22aa8-a069-11e8-98d0-529269fb1459}``` in another file ```partial-data.json```, then you can update ```data.json``` with this script so that ```data.json``` will become:


```[{title: "Formerly known as first", uuid: 24e22aa8-a069-11e8-98d0-529269fb1459},{title: "second", uuid: 24e22ec2-a069-11e8-98d0-529269fb1459},{title: "third", uuid: 24e23160-a069-11e8-98d0-529269fb1459}]```

### CLI usage

```npm run merge --sourcePath=/path/to/mergethis.json --targetPath=/path/to/mergeintothis.json --outputPath=/path/to/output.json --uuidKey=uuid```


### Usage in module

```
var merger = require('json-object-into-array-merger')

var conf = {
  sourcePath: '/path/to/mergethis.json',
  targetPath: '/path/to/mergeintothis.json',
  outputPath: '/path/to/output.json',
  uuidKey: 'uuid'
}

merger.mergeJSON(conf)
```

### Configuration options

```sourcePath```: String: path to JSON file with object to be merged

```outputPath```: String: path to file where output is saved

```targetPath```: String: path to JSON file into which object from sourcePath is merged

```uuidKey```: String: uuid key


### Tests

Run unit tests:

```npm run test```

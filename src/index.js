
import R from 'ramda'
import S from 'sanctuary'

import { mergeC, toMaybe, returnValueFromMaybe, hasValueAtPathC, isValidUuid } from './lib'
import { readFileP, writeFile, buildConfFromCommandLineOrFunctionArguments } from './io'
import { logError, logC, displayConf } from './logger'

// String -> Maybe -> String
const checkValue = R.pipe(
  toMaybe,
  R.either(returnValueFromMaybe, logError)
)

// String -> String -> Promise
const data = R.pipe(
  checkValue,
  readFileP
)

// Buffer -> String -> String -> Either -> String
const validateData = R.pipe(
  R.toString,
  checkValue,
  JSON.parse
)

// String -> Either -> String
const uuid = R.pipe(
  hasValueAtPathC,
  S.either(logError)(isValidUuid)
)

// Configuration object -> write data to file
const getDataAndMerge = async function (conf) {
  // Get data to be merged
  const sourceData = await data(checkValue(conf.sourcePath))
  const validatedSourceData = validateData(sourceData)
  // Get target data
  const targetData = await data(checkValue(conf.targetPath))
  // Merge source data into target data
  const mergedJson = mergeC(uuid(checkValue(conf.uuidKey), validatedSourceData), validateData(targetData), validatedSourceData)
  writeFile(conf.outputPath, JSON.stringify(mergedJson))
}

const mergeJSON = R.pipe(
  buildConfFromCommandLineOrFunctionArguments,
  displayConf,
  getDataAndMerge,
  logC('Script ended. Data written to output file.')
)

module.exports = {
  mergeJSON
}

// Run exported function from the command line using https://github.com/super-cache-money/make-runnable,
// see also merge script in package.json
require('make-runnable/custom')({
  printOutputFrame: false
})

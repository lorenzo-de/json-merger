
import Promise from 'bluebird'
import fs from 'fs'
import R from 'ramda'

// String path -> Promise with Buffer
const readFileP = Promise.promisify(fs.readFile, fs)

// String path to file, String content to be written -> creates file with content at path
const writeFile = (path, content) => {
  const wStream = fs.createWriteStream(path)
  wStream.write(content)
  wStream.end()
}

/* Check if command line or function arguments are present and build new conf object
 * depending on where the configuration comes from. Does minimal check whether configuration is logic.
 * Configuration set as command line arguments has precedence.
 * @param {confObj} object - optional, a configuration object
 * @returns {conf} object - new configuration object
 */
const buildConfFromCommandLineOrFunctionArguments = (confObj) => {
  const conf = {}

  if (R.is(String, process.env.npm_config_sourcePath)) conf.sourcePath = process.env.npm_config_sourcePath
  else if (R.has(confObj, 'sourcePath')) conf.sourcePath = confObj.sourcePath
  else conf.sourcePath = undefined

  if (R.is(String, process.env.npm_config_targetPath)) conf.targetPath = process.env.npm_config_targetPath
  else if (R.has(confObj, 'targetPath')) conf.targetPath = confObj.targetPath
  else conf.targetPath = undefined

  if (R.is(String, process.env.npm_config_outputPath)) conf.outputPath = process.env.npm_config_outputPath
  else if (R.has(confObj, 'outputPath')) conf.outputPath = confObj.outputPath
  else conf.outputPath = undefined

  if (R.is(String, process.env.npm_config_uuidKey)) conf.uuidKey = process.env.npm_config_uuidKey
  else if (R.has(confObj, 'uuidKey')) conf.uuidKey = confObj.uuidKey
  else conf.uuidKey = undefined

  return conf
}

module.exports = {
  readFileP,
  writeFile,
  buildConfFromCommandLineOrFunctionArguments
}

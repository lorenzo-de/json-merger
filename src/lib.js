
import R from 'ramda'
import S from 'sanctuary'
import isUuid from 'is-uuid'

// Array with JSON objects, JSON object -> Either, Left: error message, Right: new array merged with object
const mergeC = R.curry((id, array, object) => {
  if (Array.isArray(array)) {
    if (typeof object === 'object') {
      let newArray = array
      let isMerged = false
      R.forEach((obj) => {
        // Replace if object already exists
        if (obj[id] === object[id]) {
          newArray.splice(R.indexOf(obj, newArray), 1, object)
          isMerged = true
        }
      }, newArray)
      // Concat object into array if it does not exist
      if (!isMerged) newArray = R.concat([object], newArray)
      return S.Right(newArray)
    } else return S.Left('Only object can be merged!')
  } else return S.Left('Data must be array of objects!')
})

// String path, Object -> Either, Right: value at path, Left: String error message
const hasValueAtPathC = R.curry((path, object) => {
  if (R.has(path, object)) {
    const value = object[path]
    if (R.isEmpty(value) || undefined === value) return S.Left('Object does not have value at ' + path)
    else return S.Right(value)
  } else return S.Left('Object does not have property ' + path)
})

// String uuid -> Either Right uuid, Left error message
const isValidUuid = (string) => {
  if (isUuid.anyNonNil(string)) return S.Right(string)
  else return S.Left(string + ' is not a valid UUID!')
}

// String value to check for empty'sh values -> Just.Nothing if empty, Just with value otherwise
const toMaybe = value => {
  if (value === '') return S.Nothing
  return S.toMaybe(value)
}

// Maybe -> Boolean false if Maybe.Nothing, Just value otherwise
const returnValueFromMaybe = (maybe) => {
  return S.unchecked.maybe(false)((v) => { return v })(maybe)
}

module.exports = {
  mergeC,
  hasValueAtPathC,
  isValidUuid,
  toMaybe,
  returnValueFromMaybe
}

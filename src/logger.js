
import R from 'ramda'

const logError = message => {
  console.trace('Error: ' + message)
}

const logC = R.curry((message, arg) => {
  console.log('Logging: ' + message + ' ' + arg)
})

// Object configuration -> Side effect console.log configuration -> Object configuration
const displayConf = (conf) => {
  if (conf) {
    console.log('************************************************************************************')
    console.log('The following configuration settings are used:')
    console.log(`Source path: ${conf.sourcePath}`)
    console.log(`Target path: ${conf.targetPath}`)
    console.log(`Output path: ${conf.outputPath}`)
    console.log(`Uuid key: ${conf.uuidKey}`)
    console.log('************************************************************************************')
  }
  return conf
}

module.exports = { logError, logC, displayConf }

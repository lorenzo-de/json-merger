
import assert from 'assert'
import R from 'ramda'
import S from 'sanctuary'

import {
  mergeC,
  hasValueAtPathC,
  isValidUuid,
  toMaybe,
  returnValueFromMaybe
} from '../src/lib.js'

describe('unit tests', () => {
  describe('#mergeC()', () => {
    it('should replace object in array with new object having the same uuid ', (done) => {
      const arr = [{title: 'test', uuid: '545ab712-a53e-11e8-98d0-529269fb1459'}]
      const obj = {title: 'test2', uuid: '545ab712-a53e-11e8-98d0-529269fb1459'}
      const resultArray = S.unchecked.either((res) => { return false })((res) => { return res })(mergeC('uuid', arr, obj))
      assert.ok(resultArray[0].title === 'test2')
      done()
    })

    it('should return Either.Left with error message String if second argument is not object', (done) => {
      const value = S.unchecked.either((err) => { return err })((res) => { return true })(mergeC('uuid', [], 'notObject'))
      assert.ok(R.is(String, value))
      done()
    })

    it('should return Either.Left with error message String if first argument is not array', (done) => {
      const value = S.either((err) => { return err })((res) => { return true })(mergeC('uuid', 'not array', {}))
      assert.ok(R.is(String, value))
      done()
    })

    it('returned array should contain input object', (done) => {
      const arr = [{title: 'test', uuid: '545ab712-a53e-11e8-98d0-529269fb1458'}]
      const obj = {title: 'test2', uuid: '545ab712-a53e-11e8-98d0-529269fb1459'}
      const resultArray = S.either((res) => { return false })((res) => { return res })(mergeC('uuid', arr, obj))
      assert.equal(resultArray[0], R.concat([obj], arr)[0])
      done()
    })
  })

  describe('#hasValueAtPathC()', () => {
    it('should return Either.Left with error message String if object does not have path', (done) => {
      const value = S.either((err) => { return err })((res) => { return true })(hasValueAtPathC('testpath', {otherpath: 'test'}))
      assert.ok(R.is(String, value))
      done()
    })

    it('should return Either.Left with error message String if object does not have value at path', (done) => {
      const value = S.either((err) => { return err })((res) => { return true })(hasValueAtPathC('testpath', {testpath: ''}))
      assert.ok(R.is(String, value))
      done()
    })
  })

  describe('#isValidUuid()', () => {
    it('should return Either.Left with error message String if function is called with no argument ', (done) => {
      const value = S.either((err) => { return err })((res) => { return true })(isValidUuid())
      assert.ok(R.is(String, value))
      done()
    })

    it('should return Either.Left with error message String if argument is not UUID ', (done) => {
      const value = S.either((err) => { return err })((res) => { return true })(isValidUuid('this is not an UUID'))
      assert.ok(R.is(String, value))
      done()
    })

    it('should return Either.Right with argument if argument is UUID ', (done) => {
      const uuid = '2f55acb2-a53c-11e8-98d0-529269fb1459'
      const value = S.either((res) => { return false })((res) => { return res })(isValidUuid(uuid))
      assert.equal(value, uuid)
      done()
    })
  })

  describe('#toMaybe()', () => {
    it('should return Maybe', (done) => {
      assert.equal('Maybe', S.type(toMaybe('whatever')).name)
      done()
    })
  })

  describe('#returnValueFromMaybe()', () => {
    it('should return false if Maybe.Nothing', (done) => {
      assert.equal(false, returnValueFromMaybe(S.Nothing))
      done()
    })

    it('should return Just value if Maybe.Just', (done) => {
      assert.equal('value', returnValueFromMaybe(S.Just('value')))
      done()
    })
  })
})
